package de.andrena.ausbildung.einwohnerverwaltung.persistance;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.andrena.ausbildung.einwohnerverwaltung.beans.Address;
import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Person;
import de.andrena.ausbildung.einwohnerverwaltung.persistance.PersistenceFacadeFile;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministration;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministrationImpl;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

public class PersistenceFacadeFileIT {

	public static String[] names = new String[] { "Karlsruhe", "Berlin", "Ettlingen", "Muenchen" };

	public static List<City> cities;

	public static PersistenceFacadeFile persistenceFacade;

	@BeforeClass
	public static void initCityService() {

		persistenceFacade = new PersistenceFacadeFile();

		CityService cityService = CityServiceImpl.instance();

		cities = new ArrayList<>();
		for (String name : names) {
			cities.add(cityService.createCity(name));
		}
	}

	@After
	public void deleteFile() {
		File file = new File(PersistenceFacadeFile.CITIES_FILE);
		file.delete();
		CityServiceImpl.instance().removeAllCities();
	}

	@Test
	public void testSaveCity() {
		persistenceFacade.save(cities);
		File file = new File(PersistenceFacadeFile.CITIES_FILE);
		Assert.assertTrue(file.exists());

		List<City> savedCities = persistenceFacade.loadCities();
		Assert.assertEquals(cities, savedCities);
	}

	@Test
	public void testSaveCitizen() {
		CityService cityService = CityServiceImpl.instance();
		CitizenAdministration citizAdmin = CitizenAdministrationImpl.instance();

		for (String name : names) {
			cityService.createCity(name);
		}

		City city = cityService.getAllCities().get(0);
		Long cityId = city.getId();

		Person person = citizAdmin.createCitizen("Meier", "Peter", new LocalDate(1994, 2, 3));
		Address address = new Address(city, "22334", "Dingsstraße", 4);

		citizAdmin.changeRegistration(person, address);

		cityService.exportCities();

		cityService.importCities();
		List<City> cities = cityService.getAllCities();
		assertThat(cities.size(), is(4));
		assertThat(cityService.getById(cityId).containsCitizen(person), is(true));
	}

}
