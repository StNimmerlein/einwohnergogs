package de.andrena.ausbildung.einwohnerverwaltung.beans;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.andrena.ausbildung.einwohnerverwaltung.beans.Address;
import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Person;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

public class PersonTest {

	private CityService cityService = CityServiceImpl.instance();
	private Person person;
	private City city;
	private Address address;

	@Before
	public void setUp() {
		person = new Person(1, "Musterfrau", "Petra", new LocalDate(1970, 01, 03));
		city = cityService.createCity("Hamburg");
		address = new Address(city, "01234", "Peterstraße", 7);
	}

	@Test
	public void testGetPostalAddress() {
		assertTrue(person.getPostalAddress().equalsIgnoreCase("Petra Musterfrau, Adresse unbekannt"));
	}

	@Test
	public void testGetPostalAddress2() {
		person.setAddress(address);
		assertTrue(person.getPostalAddress().equalsIgnoreCase("Petra Musterfrau, Peterstraße 7, 01234 Hamburg"));
	}

	@Test
	public void testHasSameAddressAsPerson() {
		person.setAddress(address);
		Person person2 = new Person(2, "Mustermann", "Hans", new LocalDate(1965, 01, 20));
		person2.setAddress(address);
		assertTrue(person.hasSameAddressAsPerson(person2));
	}

	@Test
	public void testHaveBothNoAddress() {
		Person person2 = new Person(2, "Mustermann", "Hans", new LocalDate(1965, 01, 20));
		assertFalse(person.hasSameAddressAsPerson(person2));
	}

	@After
	public void clearCities() {
		cityService.removeAllCities();
	}
}
