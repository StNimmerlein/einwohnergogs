package de.andrena.ausbildung.einwohnerverwaltung.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.persistance.PersistenceFacade;
import de.andrena.ausbildung.einwohnerverwaltung.persistance.PersistenceFacadeFile;
import de.andrena.ausbildung.einwohnerverwaltung.utils.IdGenerator;

public class CityServiceImpl implements CityService {

	private static CityService instance;
	private PersistenceFacade persistenceFacade = new PersistenceFacadeFile();
	private Map<Long, City> citiesDirectory = new HashMap<>();

	private CityServiceImpl() {
	}

	public static CityService instance() {
		if (instance == null) {
			instance = new CityServiceImpl();
		}
		return instance;
	}

	@Override
	public City createCity(String name) {
		long id = IdGenerator.getNextId();
		City city = new City(id, name);
		citiesDirectory.put(id, city);
		return city;
	}

	@Override
	public City getById(long cityId) {
		return citiesDirectory.get(cityId);
	}

	@Override
	public void exportCities() {
		persistenceFacade.save(getAllCities());
	}

	@Override
	public List<City> getAllCities() {
		return new ArrayList<>(citiesDirectory.values());
	}

	@Override
	public void importCities() {
		List<City> cities = persistenceFacade.loadCities();
		for (City city : cities) {
			citiesDirectory.put(city.getId(), city);
		}
	}

	@Override
	public void removeAllCities() {
		citiesDirectory = new HashMap<>();
	}

}
