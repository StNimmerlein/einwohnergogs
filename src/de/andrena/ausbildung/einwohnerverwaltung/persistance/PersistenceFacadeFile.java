package de.andrena.ausbildung.einwohnerverwaltung.persistance;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.andrena.ausbildung.einwohnerverwaltung.beans.City;

public class PersistenceFacadeFile implements PersistenceFacade {

	public static final String CITIES_FILE = "resources/cities.txt";
	private static final Logger logger = LogManager.getLogger(PersistenceFacadeFile.class);

	@Override
	public void save(List<City> cities) {

		try {
			File file = new File(CITIES_FILE);
			file.createNewFile();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
			objectOutputStream.writeObject(cities);
			objectOutputStream.close();

			logger.info("Die St�dte wurden erfolgreich in die Datei [" + file.getAbsolutePath() + "] exportiert.");

		} catch (Exception e) {

			logger.error("Beim Expoertiren von St�dten ist ein Fehler aufgetreten.");
			logger.debug(e.getMessage());
			logger.debug(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<City> loadCities() {

		List<City> cities = new ArrayList<City>();
		try {
			File file = new File(CITIES_FILE);

			if (file.exists()) {
				ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
				cities = (ArrayList<City>) objectInputStream.readObject();
				objectInputStream.close();
			}
		} catch (Exception e) {
			logger.debug(e);
		}

		return cities;
	}

}
