package de.andrena.ausbildung.einwohnerverwaltung.persistance;

import java.util.List;

import de.andrena.ausbildung.einwohnerverwaltung.beans.City;

public interface PersistenceFacade {

	void save(List<City> cities);

	List<City> loadCities();
}
