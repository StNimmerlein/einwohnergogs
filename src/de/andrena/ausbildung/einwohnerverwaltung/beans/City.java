
package de.andrena.ausbildung.einwohnerverwaltung.beans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author aosintseva
 *
 */
public class City implements Serializable {

	private static final long serialVersionUID = -2254416759612935325L;
	public static final City NO_CITY = new City(-1, "NO CITY");
	private long id;
	private String name;
	private Set<Citizen> citizens = new HashSet<>();

	public City(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Set<Citizen> getCitizens() {
		return citizens;
	}

	public boolean containsCitizen(Person citizen) {
		return citizens.contains(citizen);
	}

	public boolean addCitizen(Person citizen) {
		return citizens.add(citizen);
	}

	public boolean removeCitizen(Person citizen) {
		return citizens.remove(citizen);
	}

	public String getName() {
		return name;
	}

	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		City other = (City) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "City [id=" + id + ", name=" + name + "]";
	}

}
